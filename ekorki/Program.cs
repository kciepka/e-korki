﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows.Forms;
using System.Reflection;
using System.Configuration;
using Microsoft.Win32;

namespace ekorki
{
    class Program
    {
        private readonly static string currentUserRunRegistryKey = "SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run";
        private readonly static string _login = ConfigurationManager.AppSettings["login"];
        private readonly static string _password = ConfigurationManager.AppSettings["password"];
        private readonly static string _interval = ConfigurationManager.AppSettings["interval_ms"];

        static void Main(string[] args)
        {
            AddAppToWindowsRunRegistry("ekorki");
            StartLoginTask();
            SendPost().Wait();
            Application.Run();
        }

        public static async Task SendPost()
        {
            HttpClient client = new HttpClient();

            var values = new Dictionary<string, string>
            {
                { "login", _login },
                { "passwd", _password },
                {"post_check", "login_form" },
                {"logme", "1" },
            };

            var content = new FormUrlEncodedContent(values);

            var response = await client.PostAsync("http://www.e-korepetycje.net/zaloguj", content);
            var responseString = response.Content.ReadAsStringAsync();

            var response_logout = await client.GetAsync("http://www.e-korepetycje.net/wyloguj");
            var response_logoutString = response_logout.Content.ReadAsStringAsync();
        }

        public static void StartLoginTask()
        {
            System.Timers.Timer checkForTime = new System.Timers.Timer(int.Parse(_interval)); // 15mins
            checkForTime.Elapsed += new ElapsedEventHandler(timer_handler);
            checkForTime.Enabled = true;
        }

        private static void timer_handler(object sender, ElapsedEventArgs e)
        {
            SendPost().Wait();
        }

        public static void AddAppToWindowsRunRegistry(string name)
        {
            string windowsRunRegistryKey = currentUserRunRegistryKey;
            AddCurrentUserRegistryKey(windowsRunRegistryKey, name, Assembly.GetExecutingAssembly().Location);
        }

        private static void AddCurrentUserRegistryKey(string parentKey, string keyName, string keyValue)
        {
            try
            {
                RegistryKey rkApp = Registry.CurrentUser.OpenSubKey(parentKey, true);
                if (rkApp != null)
                {
                    var regKeyValue = rkApp.GetValue(keyName);
                    if (regKeyValue == null)
                    {
                        rkApp.SetValue(keyName, keyValue);
                    }
                    else if (regKeyValue.ToString() != keyValue)
                    {
                        rkApp.SetValue(keyName, keyValue);
                    }
                }
                else
                {
                }
            }
            catch (Exception ex)
            {
            }
        }
    }
        
    
}
